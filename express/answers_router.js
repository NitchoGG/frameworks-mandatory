module.exports = (mongoose) => {
    let express = require('express');
    let router = express.Router();

    let answerSchema = mongoose.Schema({
        text: String,
        author: String,
        score : Number,
        questionId : String
    });

    let answerModel = mongoose.model("answer",answerSchema);

    /****** Routes *****/
    router.get('/', (req, res) => {
        answerModel.find({}).exec((err,answers) =>{
            if(err) return console.log(err);
            res.json(answers);
        })
    });

    router.get('/:id', (req, res) => {
        answerModel.findOne({_id:req.params.id},(err,answers) => {
            if(err) return console.log(err);
            res.json(answers);
        })
    });

    router.get('/get/:id', (req, res) => {
        answerModel.find({questionId:req.params.id},(err,answers) => {
            if(err) return console.log(err);
            res.json(answers);
        })
    });

    router.post('/', (req, res) => {
        // Define the task object
        let tempAnswer = new answerModel({
            text: req.body.text,
            author : req.body.author,
            score : req.body.score,
            questionId: req.body.questionId
        });

        tempAnswer.save((err) =>{
            if(err) console.log(err);
        });
        // Return a message and the new task object
        res.json({msg: "Answer created"});
    });

    router.put('/:id', (req, res) => {
        let temp = new answerModel(req.body);
        console.log(temp);
        answerModel.findOne({"_id":req.params.id}).exec((err,answer) =>{
            if(err) return console.log(err);

            if(answer){
                answer.text = temp.text;

                answer.save();
                res.json({msg:"Updated"});
            }
            else{
                res.json({msg:"Failed to update"});
            }
        });
    });


    router.put('/score/:id/:action', (req, res) => {
        answerModel.findOne({"_id":req.params.id}).exec((err,answer) =>{
            if(err) return console.log(err);
            let temp = new answerModel(req.body);
            if(answer){
                if(req.params.action === "+")
                {
                    answer.score = temp.score + 1;
                }
                else{
                    answer.score = temp.score - 1;
                }

                answer.save();
                res.json({msg:"Updated"});
            }
            else{
                res.json({msg:"Failed to update"});
            }
        });
    });

    router.delete('/:id',(req,res) =>{
        answerModel.deleteOne({"_id" : req.params.id},(err) =>{
            if(err) return console.log(err);

            res.json("Deleted Question")
        })
    });

    router.delete('/questionId/:id',(req,res) =>{
        answerModel.deleteMany({"questionId" : req.params.id},(err) =>{
            if(err) return console.log(err);

            res.json("Deleted answers to the question")
        })
    });

    return router;
};