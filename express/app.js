/**** External libraries ****/
const path = require('path');
const express = require('express');
const bodyParser = require('body-parser');
const morgan = require('morgan');
const checkJwt = require('express-jwt');    // Check for access tokens automatically
const bcrypt = require('bcrypt');           // Used for hashing passwords!
const mongoose = require('mongoose');
const pathToRegexp = require('path-to-regexp')

/**** Configuration ****/
const appName = "stackunderflowApi";
const port = (process.env.PORT || 8080);
const app = express();
app.use(bodyParser.json()); // Parse JSON from the request body
app.use(morgan('combined')); // Log all requests to the console
app.use(express.static(path.join(__dirname, '../build')));

if (!process.env.JWT_SECRET) {
    console.error('You need to put a secret in the JWT_SECRET env variable!');
    process.exit(1);
}

// Additional headers for the response to avoid trigger CORS security
// errors in the browser
// Read more here: https://en.wikipedia.org/wiki/Cross-origin_resource_sharing
app.use((req, res, next) => {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Authorization, Origin, X-Requested-With, Content-Type, Accept");
    res.header("Access-Control-Allow-Methods", "GET, POST, OPTIONS, PUT, DELETE");

    // intercepts OPTIONS method
    if ('OPTIONS' === req.method) {
      // respond with 200
      console.log("Allowing OPTIONS");
      res.send(200);
    }
    else {
      // move on
      next();
    }
});

// Open paths that does not need login

let openPaths = function(req){
    if(req.path === '/api/users/authenticate') return true;
    else if(req.method === "GET")
    {
        if(pathToRegexp('/api/answers/get/:id').test(req.path)) return true;
        else if(pathToRegexp('/question/:id').test(req.path)) return true;
        else if(pathToRegexp('/allAnswers').test(req.path)) return true;
        else if(pathToRegexp('/ask').test(req.path)) return true;
        else if(pathToRegexp('/login').test(req.path)) return true;
        else if(pathToRegexp('/editQuestion').test(req.path)) return true;
        else if(pathToRegexp('/editAnswer').test(req.path)) return true;
        else if(pathToRegexp('/question/tag/:tag').test(req.path)) return true;
        else if(pathToRegexp('/createUser').test(req.path)) return true;
        else if(pathToRegexp('/api/questions/:id').test(req.path)) return true;
        else if(req.path === '/api/questions') return true;
        else if(req.path === '/api/answers') return true;
    }
    else if(req.method ==="PUT")
    {
        if(pathToRegexp('/api/questions/score/:id/:action').test(req.path)) return true;
        else if(pathToRegexp('/api/answers/score/:id/:action').test(req.path)) return true;
        else if(pathToRegexp('/api/questions/views/:id').test(req.path)) return true;
    }
    else if(req.method ==="POST")
    {
        if(req.path==='/api/users') return true;
    }
    return false;
};

// Validate the user using authentication
app.use(
    checkJwt({ secret: process.env.JWT_SECRET }).unless(openPaths)
);
app.use((err, req, res, next) => {
    if (err.name === 'UnauthorizedError') {
        res.status(401).json({ error: err.message });
    }
});

/**** Mongo *****/
mongoose.connect('mongodb://dbuser:niels2502@5.186.60.140:27017/stackunderflow_n?authSource=admin',{useNewUrlParser: true});

let db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', function() {
    console.log("DB connection is open!");
});


/**** Routes ****/
let questionRouter = require('./questions_router')(mongoose);
app.use('/api/questions', questionRouter);

let usersRouter = require('./users_router')(mongoose);
app.use('/api/users', usersRouter);

let answerRouter = require('./answers_router')(mongoose);
app.use('/api/answers', answerRouter);

/**** Reroute all unknown requests to the React index.html ****/
app.get('/*', (req, res) => {
  res.sendFile(path.join(__dirname, '../build/index.html'));
});

/**** Start! ****/
app.listen(port, () => console.log(`${appName} API running on port ${port}!`));




