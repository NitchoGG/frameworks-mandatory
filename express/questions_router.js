module.exports = (mongoose) => {
    let express = require('express');
    let router = express.Router();
    let moment = require('moment');

    let questionSchema = mongoose.Schema({
        title: String,
        questiontext: String,
        author : String,
        score: Number,
        tags:Array,
        last_updated:String,
        last_updatedDate:Date,
        views:Number,
        updated : Boolean
    });

    let questionModel = mongoose.model("question",questionSchema);

    /****** Routes *****/
    router.get('/', (req, res) => {
        questionModel.find({}).sort({'last_updatedDate' : 'descending'}).exec((err,questions) =>{
            if(err) return console.log(err);
            res.json(questions);
        })
    });

    router.get('/:id', (req, res) => {
        questionModel.findOne({_id:req.params.id},(err,question) => {
            if(err) return console.log(err);
            res.json(question);
        })
    });

    router.post('/', (req, res) => {
        // Define the task object

        let date = new Date();
        date.setUTCHours(date.getUTCHours() + 2);
        let datestring = date.getDate() + '-' + (date.getUTCMonth() + 1) + '-' + date.getUTCFullYear() + ' ' + date.getUTCHours() + ':' + date.getUTCMinutes() + ':' + date.getUTCSeconds();
        let tempQuestion = new questionModel({
            title: req.body.title,
            questiontext : req.body.questiontext,
            author : req.body.author,
            score: req.body.score,
            tags: req.body.tags,
            last_updated: datestring,
            last_updatedDate : Date.now(),
            views: req.body.views,
            updated : false
        });

        tempQuestion.save((err) =>{
            if(err) console.log(err);
        });
        // Return a message and the new task object
        res.json({msg: "Question created"});
    });

    router.put('/:id', (req, res) => {
        let date = new Date();
        date.setUTCHours(date.getUTCHours() + 2);
        let datestring = date.getDate() + '-' + (date.getUTCMonth() + 1) + '-' + date.getUTCFullYear() + ' ' + date.getUTCHours() + ':' + date.getUTCMinutes() + ':' + date.getUTCSeconds();
        let temp = new questionModel(req.body);
        questionModel.findOne({"_id":req.params.id}).exec((err,question) =>{
            if(err) return console.log(err);

            if(question){
                question.title = temp.title;
                question.questiontext = temp.questiontext;
                question.author = temp.author;
                question.tags = temp.tags;
                question.last_updated = datestring;
                question.last_updatedDate = Date.now();
                question.updated = true;

                question.save();
                res.json({msg:"Updated"});
            }
            else{
                res.json({msg:"Failed to update"});
            }
        });
    });

    router.put('/views/:id', (req, res) => {
        questionModel.findOne({"_id":req.params.id}).exec((err,question) =>{
            if(err) return console.log(err);
            if(question){
                question.views = question.views + 1;
                question.save();
                res.json({msg:"Updated"});
            }
            else{
                res.json({msg:"Failed to update"});
            }
        });
    });

    router.put('/score/:id/:action', (req, res) => {
        questionModel.findOne({"_id":req.params.id}).exec((err,question) =>{
            if(err) return console.log(err);
            let temp = new questionModel(req.body);
            if(question){
                if(req.params.action === "+")
                {
                    question.score = temp.score + 1;
                }
                else{
                    question.score = temp.score - 1;
                }

                question.save();
                res.json({msg:"Updated"});
            }
            else{
                res.json({msg:"Failed to update"});
            }
        });
    });

    router.delete('/:id',(req,res) =>{
        questionModel.deleteOne({"_id" : req.params.id},(err) =>{
            if(err) return console.log(err);

            res.json("Deleted Question")
        })
    });

    return router;
};