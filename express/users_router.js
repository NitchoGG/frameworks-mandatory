module.exports = (mongoose) => {
    let express = require('express');
    let router = express.Router();

    const jwt = require('jsonwebtoken');
    const bcrypt = require('bcrypt');

    let userSchema = mongoose.Schema({
        username: String,
        password: String
    });

    let userModel = mongoose.model("user", userSchema);

    router.post('/', (req, res) => {
        userModel.find({username: req.body.username}, function (err, user) {
            if (user.length) {
                res.status(401).json({msg: "Username is already taken"})
            } else {
                bcrypt.hash(req.body.password, 10, function (err, hash) {
                    let tempUser = new userModel({username: req.body.username, password: hash});
                    tempUser.save((err, result) => {
                        if (err) console.log(err);
                        else if (result) {
                            res.json({msg: "User Created!"});
                        }
                    })
                });
            }
        });
    });

    router.post('/authenticate', (req, res) => {
        const username = req.body.username;
        const password = req.body.password;

        if (!username || !password) {
            let msg = "Username or password missing!";
            console.error(msg);
            res.status(401).json({msg: msg});
            return;
        }

        const user = userModel.findOne({username: username}, (err, user) => {
            if (user) {
                bcrypt.compare(password, user.password, (err, result) => {
                    if (result) {
                        const payload = {
                            username: username,
                            admin: false
                        };
                        const token = jwt.sign(payload, process.env.JWT_SECRET, {expiresIn: '1h'});

                        res.json({
                            msg: 'User authenticated successfully',
                            token: token
                        });
                    } else res.status(401).json({msg: "Password mismatch!"})
                });
            } else {
                res.status(404).json({msg: "User not found!"});
            }
        });
    });

    return router;
};