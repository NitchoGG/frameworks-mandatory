import React, {Component} from 'react';
import { Link, Redirect } from 'react-router-dom';

export default class Answer extends Component {

    constructor(props) {
        super(props);
        this.state = {
            score : this.props.answer.score
        };
        this.handleUpdateAnswerScore = this.handleUpdateAnswerScore.bind(this);
        this.deleteAnswer = this.deleteAnswer.bind(this);
        this.editAnswer = this.editAnswer.bind(this);
    }

    handleUpdateAnswerScore(event){
        this.props.updateScore(this.props.answer,this.props.answer._id,event.target.innerHTML,this.props.questionId);
    }

    deleteAnswer(event){
        this.props.deleteAnswer(this.props.answer._id,this.props.answer.questionId);
    }

    editAnswer(event){
        this.props.setAnswer(this.props.answer);
    }

    render() {
        let edit = [];
        let user = localStorage.getItem('username');

        if (this.props.answer.author === user) {
            edit.push(
                <div className={"editContainer"}>
                    <button className={"btn btn-primary"} onClick={this.deleteAnswer}>Delete</button>
                    <Link to={"/editAnswer"} className={"btn btn-primary"} onClick={this.editAnswer}> Edit </Link>
                </div>
            )
        }
        return (
                <div className="row headerQuestion">
                        <div className="col-1">
                            <div className="updateViews">
                                <button onClick={this.handleUpdateAnswerScore} >+</button>
                                <h3>{this.props.answer.score}</h3>
                                <button onClick={this.handleUpdateAnswerScore}>-</button>
                            </div>
                        </div>
                        <div className="col-11 questionTop">
                            {edit}
                            <p className={"break"}>{this.props.answer.text}</p>
                            <p className="questionTime">Answered by : {this.props.answer.author}</p>
                        </div>
                    </div>
        );
    }
}