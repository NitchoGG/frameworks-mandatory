import React, {Component} from 'react';
import {Link} from 'react-router-dom';
import QuestionHeader from "./QuestionHeader";
import AnswerListItem from "./AnswerListItem"

export default class AnswerList extends Component {

    constructor(props) {
        super(props);
        this.props.getAnswers();
    }

    render() {
        let list = [];

        let content =
            <div className="container">
                <h3>Please login to continue viewing</h3>
            </div>;

        if (this.props.answers) {
            this.props.answers.forEach((elm) => {
                list.push(<AnswerListItem key={elm._id} answer={elm} Auth={this.props.Auth}
                                          API_URL={this.props.API_URL}/>);
            });
        }

        content =
            <div className="container questions">
                <QuestionHeader title={"List of all Answers"} buttonText={"Ask a question"}/>
                {list}
            </div>;


        return content;
    }
}