import React, {Component} from 'react';
import { Link } from 'react-router-dom';
import AuthService from "./AuthService";
export default class AnswerListItem extends Component {

    constructor(props) {
        super(props);

        this.state = {
            title : ""
        };

        this.props.Auth.fetch(`${this.props.API_URL}/questions/` + this.props.answer.questionId)
            .then(question => {
                this.setState({
                    title : question.title
                })
            })
            .catch(error => {
                // TODO: Inform the user about the error
                console.error("Error when fetching: ", error);
            });
    }

    componentDidMount() {
    }

    render() {
        let answerList =[];

        console.log(this.props.answer);

        if(this.props.answer)
        {

            answerList.push(
                <Link to={`/question/${this.props.answer.questionId}`}>
                    <h3 className={"break"}>Question Thread : {this.state.title}</h3>
                    <p className={"break"}>Answer : {this.props.answer.text}</p>
                </Link>
            );
        }
        return (
            <div className="row question">
                <div className="col-12">
                    {answerList}
                </div>
            </div>
        );
    }
}