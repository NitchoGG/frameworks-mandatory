import React, {Component} from 'react';
import {BrowserRouter as Router, Switch, Route, Link} from 'react-router-dom';

import './App.css';
import Navbar from './Navbar'
import Login from './Login'
import QuestionList from './QuestionList'
import Question from './Question'
import AskQuestion from './AskQuestion'
import AuthService from './AuthService'
import AnswerList from './AnswerList'
import CreateUser from './CreateUser'
import EditQuestion from './EditQuestion'
import EditAnswer from './EditAnswer'

class App extends Component {
    API_URL = process.env.REACT_APP_API_URL;

    constructor(props) {
        super(props);

        this.Auth = new AuthService(`${this.API_URL}/users/authenticate`);
        this.state = {
            API: "",
            isLoggedIn: false,
            questions: [],
            specificQuestion: "",
            specificAnswer: "",
            answers: [],
            allAnswers: [],
            userCreated: false,
            userCreatedResponse: "",
            updatedQuestion : false

        };

        this.getData = this.getData.bind(this);
        this.getAnswers = this.getAnswers.bind(this);
        this.getQuestionFromId = this.getQuestionFromId.bind(this);
        this.askQuestion = this.askQuestion.bind(this);
        this.addComment = this.addComment.bind(this);
        this.updateViews = this.updateViews.bind(this);
        this.updateScore = this.updateScore.bind(this);
        this.addAnswer = this.addAnswer.bind(this);
        this.getAnswersFromId = this.getAnswersFromId.bind(this);
        this.updateAnswerScore = this.updateAnswerScore.bind(this);
        this.setLoggedIn = this.setLoggedIn.bind(this);
        this.filterByTag = this.filterByTag.bind(this);
        this.CreateUser = this.CreateUser.bind(this);
        this.setUserChanged = this.setUserChanged.bind(this);
        this.updateQuestion = this.updateQuestion.bind(this);
        this.setUpdateQuestionChanged = this.setUpdateQuestionChanged.bind(this);
        this.deleteQuestion = this.deleteQuestion.bind(this);
        this.deleteAnswer = this.deleteAnswer.bind(this);
        this.setAnswer = this.setAnswer.bind(this);
        this.updateAnswer = this.updateAnswer.bind(this);
    }

    componentDidMount() {
        if(this.state.isLoggedIn === false)
        {
            let token = this.Auth.getToken();
            if (token) {
                this.setState({isLoggedIn: true})
            }
        }
    }

    getData() {
        this.Auth.fetch(`${this.API_URL}/questions`)
            .then(questions => {
                this.setState({
                    questions: questions
                });
            })
            .catch(error => {
                // TODO: Inform the user about the error
                console.error("Error when fetching: ", error);
            })
    }

    getAnswers() {
        this.Auth.fetch(`${this.API_URL}/answers`)
            .then(answers => {
                this.setState({
                    allAnswers: answers
                });
            })
            .catch(error => {
                // TODO: Inform the user about the error
                console.error("Error when fetching: ", error);
            })
    }

    setLoggedIn(state) {
        if (state) {
            this.setState({isLoggedIn: true});
        } else {
            this.setState({isLoggedIn: false});
        }
    }

    getQuestionFromId(id) {
        this.Auth.fetch(`${this.API_URL}/questions/` + id)
            .then(question => {
                this.setState({
                    specificQuestion: question
                });
            })
            .catch(error => {
                // TODO: Inform the user about the error
                console.error("Error when fetching: ", error);
            })
    }

    getAnswersFromId(id) {
        this.Auth.fetch(`${this.API_URL}/answers/get/` + id)
            .then(answers => {
                this.setState({
                    answers: answers
                });
            })
            .catch(error => {
                // TODO: Inform the user about the error
                console.error("Error when fetching: ", error);
            })
    }

    askQuestion(item) {
        this.Auth.fetch(`${this.API_URL}/questions/`, {
            method: 'POST',
            body: JSON.stringify(item)
        }).then(res => {
            this.getData();
        })
    }

    CreateUser(user) {
        this.Auth.fetch(`${this.API_URL}/users`, {
            method: 'POST',
            body: JSON.stringify(user)
        }).then(res => {
            console.log(res.msg);
            if (res.msg === "User Created!") {
                this.setState({
                    userCreated: true
                })
            }
            this.setState({
                userCreatedResponse: res.msg
            })
        })
    }

    setUserChanged() {
        this.setState({
            userCreated: false,
            userCreatedResponse: ""
        })
    }

    setUpdateQuestionChanged(){
        this.setState({
            updatedQuestion: false
        })
    }

    addComment(item, id) {
        this.Auth.fetch(`${this.API_URL}/questions/` + id, {
            method: 'PUT',
            body: JSON.stringify(item)
        }).then(res => {
            this.getQuestionFromId(id);
        })
    }

    addAnswer(item, id) {
        this.Auth.fetch(`${this.API_URL}/answers`, {
            method: 'POST',
            body: JSON.stringify(item)
        }).then(res => {
            this.getAnswersFromId(id);
        })
    }

    updateQuestion(item, id) {
        this.Auth.fetch(`${this.API_URL}/questions/` + id, {
            method: 'PUT',
            body: JSON.stringify(item)
        }).then(res => {
            this.getQuestionFromId(id);
            this.setState({
                updatedQuestion: true
            })
        })
    }

    updateAnswer(item, id,questionId) {
        this.Auth.fetch(`${this.API_URL}/answers/` + id, {
            method: 'PUT',
            body: JSON.stringify(item)
        }).then(res => {
            this.getAnswersFromId(questionId);
        })
    }

    updateViews(item, id) {
        this.Auth.fetch(`${this.API_URL}/questions/views/` + id, {
            method: 'PUT',
            body: JSON.stringify(item)
        }).then(res => {
            this.getQuestionFromId(id);
        })
    }

    updateScore(item, id, action) {
        this.Auth.fetch(`${this.API_URL}/questions/score/${id}/` + action, {
            method: 'PUT',
            body: JSON.stringify(item)
        }).then(res => {
            this.getQuestionFromId(id);
        })
    }

    updateAnswerScore(item, id, action, questionId) {
        this.Auth.fetch(`${this.API_URL}/answers/score/${id}/` + action, {
            method: 'PUT',
            body: JSON.stringify(item)
        }).then(res => {
            this.getAnswersFromId(questionId);
        })
    }

    deleteQuestion(id){
        this.Auth.fetch(`${this.API_URL}/questions/${id}`, {
            method: 'DELETE',
        }).then(res => {
            this.getData();
        });
        this.Auth.fetch(`${this.API_URL}/answers/questionId/${id}`, {
            method: 'DELETE',
        }).then(res => {
            this.getAnswers();
        })
    }

    deleteAnswer(id, questionId){
        this.Auth.fetch(`${this.API_URL}/answers/${id}`, {
            method: 'DELETE',
        }).then(res => {
            this.getAnswersFromId(questionId);
        });
    }

    setAnswer(answer){
        this.setState({
            specificAnswer : answer
        })
    }

    filterByTag(tag) {
        return this.state.questions.filter((elm) => elm.tags.includes(tag))
    }

    render() {
        return (
            <Router>
                <div>
                    <Navbar isLoggedIn={this.state.isLoggedIn}/>
                    <div className="container-fluid contentSpacing">
                        <Switch>
                            <Route exact path="/" render={(props) =>
                                <QuestionList key={"questionList"} {...props}
                                              questions={this.state.questions}
                                              getQuestions={this.getData}
                                              Auth={this.Auth}
                                              API_URL={this.API_URL}
                                />}/>
                            <Route exact path="/login" render={(props) =>
                                <Login {...props}
                                       Auth={this.Auth}
                                       API={this.API_URL}
                                       isLoggedIn={this.state.isLoggedIn}
                                       ChangeLoginState={this.setLoggedIn}
                                       username={this.state.username}
                                />}/>
                            <Route exact path={'/question/:id'} render={(props) =>
                                <Question {...props}
                                          getQuestion={this.getQuestionFromId}
                                          state={this.state.specificQuestion}
                                          answers={this.state.answers}
                                          id={props.match.params.id}
                                          key={props.match.params.id}
                                          addComment={this.addComment}
                                          isLoggedIn={this.state.isLoggedIn}
                                          updateViews={this.updateViews}
                                          updateScore={this.updateScore}
                                          updateAnswerScore={this.updateAnswerScore}
                                          addAnswer={this.addAnswer}
                                          getAnswers={this.getAnswersFromId}
                                          deleteQuestion = {this.deleteQuestion}
                                          deleteAnswer = {this.deleteAnswer}
                                          setAnswer = {this.setAnswer}
                                />}/>
                            <Route exact path="/ask" render={(props) =>
                                <AskQuestion {...props}
                                             askQuestion={this.askQuestion}
                                             isLoggedIn={this.state.isLoggedIn}
                                />}/>
                            <Route exact path="/AllAnswers" render={(props) =>
                                <AnswerList {...props}
                                            answers={this.state.allAnswers}
                                            getAnswers={this.getAnswers}
                                            Auth={this.Auth}
                                            API_URL={this.API_URL}
                                            isLoggedIn={this.state.isLoggedIn}
                                />}/>
                            <Route exact path="/question/tag/:tag" render={(props) =>
                                <QuestionList key={"questionListWithTag"} {...props}
                                              questions={this.filterByTag(props.match.params.tag)}
                                              getQuestions={this.getData}
                                              Auth={this.Auth}
                                              API_URL={this.API_URL}
                                              header={props.match.params.tag}
                                />}/>

                            <Route exact path="/createUser" render={(props) =>
                                <CreateUser {...props}
                                            CreateUser={this.CreateUser}
                                            userCreated={this.state.userCreated}
                                            userCreatedResponse={this.state.userCreatedResponse}
                                            setUserChanged={this.setUserChanged}
                                />}/>

                            <Route exact path="/editQuestion" render={(props) =>
                                <EditQuestion {...props}
                                            state={this.state.specificQuestion}
                                              isLoggedIn={this.state.isLoggedIn}
                                              updatedQuestion = {this.state.updatedQuestion}
                                              updateQuestion = {this.updateQuestion}
                                              setUpdateQuestion = {this.setUpdateQuestionChanged}
                                />}/>

                            <Route exact path="/editAnswer" render={(props) =>
                                <EditAnswer {...props}
                                            answer = {this.state.specificAnswer}
                                              isLoggedIn={this.state.isLoggedIn}
                                            updateAnswer = {this.updateAnswer}

                                />}/>
                        </Switch>
                    </div>
                </div>
            </Router>
        );
    }
}

export default App;
