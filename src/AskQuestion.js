import React, {Component} from 'react';
import { Link, Redirect } from 'react-router-dom';

export default class AskQuestion extends Component {

    constructor(props) {
        super(props);
        this.state = {
            title: "",
            questiontext: "",
            tagTitle: [],
            toDashboard : false
        };

        this.handleOnChange = this.handleOnChange.bind(this);
        this.onClickSubmit = this.onClickSubmit.bind(this);
    }

    onClickSubmit(event){
        event.preventDefault();

        if(!this.props.isLoggedIn)
        {
            this.lblLogin.innerHTML = "You need to login to post a comment";
        }
        else
        {
            let question = {
                title : this.state.title,
                questiontext : this.state.questiontext,
                author : localStorage.getItem('username'),
                comments: [],
                score : 0,
                tags : this.state.tagTitle,
                views : 0
            };

            this.props.askQuestion(question);
            this.setState({
                toDashboard : true
            });
        }
    }

    handleOnChange(event) {
        switch (event.target.id) {
            case "inputTitle":
                this.setState({
                    title: event.target.value
                });
                break;
            case "inputBody":
                this.setState({
                    questiontext: event.target.value
                });
                break;
            case "inputTags":
                let tags = event.target.value.toLowerCase().split(',');
                this.setState({
                    tagTitle : tags
                });
                break;
            default:
                console.log(event.target.id);
                break;
        }
    }

    render() {
        if (this.state.toDashboard === true) {
            return <Redirect to='/' />
        }
        return (
            <div className="container">
                <div className="row">
                    <div className="col-12">
                        <form>
                            <h3>Ask a question</h3>
                            <p>Title</p>
                            <input className="askInput" onChange={this.handleOnChange} id="inputTitle" placeholder={"Add your title"}/>
                            <p>Body</p>
                            <textarea className="asktextareaInput" onChange={this.handleOnChange} id="inputBody" placeholder={"Describe your problem in details"}/>
                            <p>Tags (seperate with ,)</p>
                            <input className="askInput inputPlaceholder" onChange={this.handleOnChange} id="inputTags" placeholder={"C#,React,Javascript"}/>
                            <Link to={"/"} className="btn btn-primary" onClick={this.onClickSubmit}>Post your Question</Link>
                            <label className="loginError" ref={label => this.lblLogin = label}> </label>
                        </form>
                    </div>
                </div>
            </div>
        );
    }
}