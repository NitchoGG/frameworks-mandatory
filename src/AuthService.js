/**
 * Service class for authenticating users against an API
 * and storing JSON Web Tokens in the browsers LocalStorage.
 */
import React, {Component} from 'react';
import jwtDecode from 'jwt-decode';
class AuthService {

    constructor(auth_api_url) {
        this.auth_api_url = auth_api_url;
        this.fetch = this.fetch.bind(this);
        this.login = this.login.bind(this);
    }

    login(username, password) {
        return this.fetch(this.auth_api_url, {
            method: 'POST',
            body: JSON.stringify({
                username,
                password
            })
        }).then(res => {
            console.log(res.msg);
            if(res.msg === "User authenticated successfully")
            {
                this.setToken(res.token,username);
            }
            else{

            }
            return Promise.resolve(res);
        })
    }

    loggedIn() {
        // TODO: Check if token is expired using 'jwt-decode'
        // TODO: npm install jwt-decode
        let token = this.getToken();
        if(token != null)
        {
            if(token === ""){
                this.logout();
            }
            else if (jwtDecode(this.getToken()).exp < Date.now() / 1000) {
                this.logout();
            }
        }
        return (this.getToken() !== undefined);
    }

    setToken(token,username) {
        localStorage.setItem('token', token);
        localStorage.setItem('username', username);
    }

    getToken() {
        return localStorage.getItem('token')
    }

    logout() {
        localStorage.removeItem('token');
        localStorage.removeItem('username');
    }


    fetch(url, options) {
        const headers = {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        };

        if (this.loggedIn()) {
            headers['Authorization'] = 'Bearer ' + this.getToken()
        }

        return fetch(url, {
            headers,
            ...options
        })
            .then(response => response.json());
    }
}

export default AuthService;
