import React, {Component} from 'react';
import {Link} from "react-router-dom";


export default class CreateUser extends Component {
    constructor(props) {
        super(props);
        this.props.setUserChanged();


        this.createUser = this.createUser.bind(this);
    }

    createUser (event){
        event.preventDefault();

        let user = {
            username: event.target.username.value.toLowerCase(),
            password: event.target.password.value
        };

        this.props.CreateUser(user);

    }

    render() {
        let label = "";
        if(this.props.userCreatedResponse)
        {
            label = <label className="loginErrorBottom" hidden={false}>{this.props.userCreatedResponse}</label>
        }
        else{
            label = <label className="loginErrorBottom" hidden={true}> </label>;
        }

        let content =
            <div className="loginContent container">
                <form className="loginForm" onSubmit={this.createUser}>
                    <h2>Sign up</h2>
                    <input type="text" id="username" name="username" placeholder="username"/>
                    <input type="password" id="password" name="password" placeholder="password"/>
                    <input className="btn btn-primary" type="submit" value="Create User"/>
                    {label}
                </form>
            </div>;

        if(this.props.userCreated)
        {
            content = <p>{this.props.userCreatedResponse} You can now login at the top right</p>
        }

        return content;

    }
}

