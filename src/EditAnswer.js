import React, {Component} from 'react';
import { Link, Redirect } from 'react-router-dom';

export default class EditAnswer extends Component {

    constructor(props) {
        super(props);

        this.onClickSubmit = this.onClickSubmit.bind(this);
    }

    onClickSubmit(event){
        event.preventDefault();
        let update = {
            text : event.target.inputBody.value,
        };

        this.props.updateAnswer(update,this.props.answer._id,this.props.answer.questionId);

        this.props.history.goBack();
    }

    render() {
        if (!this.props.isLoggedIn) {
            return <Redirect to='/' />
        }

        let content = <p>Loading</p>

        if(this.props.answer) {
            content =
                <div className="container">
                    <div className="row">
                        <div className="col-12">
                            <form onSubmit={this.onClickSubmit}>
                                <h3>Edit Answer</h3>
                                <p>Body</p>
                                <textarea className="asktextareaInput" id="inputBody"
                                          defaultValue={this.props.answer.text}/>
                                <input className="btn btn-primary" type="submit" value="Update your Answer"/>
                                <label className="loginError" ref={label => this.lblLogin = label}> </label>
                            </form>
                        </div>
                    </div>
                </div>;
        }
        return content;
    }
}