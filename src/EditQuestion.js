import React, {Component} from 'react';
import { Link, Redirect } from 'react-router-dom';

export default class EditQuestion extends Component {

    constructor(props) {
        super(props);

        this.props.setUpdateQuestion();
        this.onClickSubmit = this.onClickSubmit.bind(this);
    }

    onClickSubmit(event){
        event.preventDefault();
        let update = {
            title : event.target.inputTitle.value,
            questiontext : event.target.inputBody.value,
            author : localStorage.getItem('username'),
            tags : event.target.inputTags.value.toLowerCase().split(','),
        };

        this.props.updateQuestion(update,this.props.state._id);
    }

    render() {
        let url = "/question/" + this.props.state._id;
        if (!this.props.isLoggedIn) {
            return <Redirect to='/' />
        }
        else if(this.props.updatedQuestion)
        {
            return <Redirect to={url}  />
        }
        return (
            <div className="container">
                <div className="row">
                    <div className="col-12">
                        <form onSubmit={this.onClickSubmit}>
                            <h3>Edit question</h3>
                            <p>Title</p>
                            <input className="askInput" id="inputTitle" defaultValue={this.props.state.title} />
                            <p>Body</p>
                            <textarea className="asktextareaInput" id="inputBody" defaultValue={this.props.state.questiontext}/>
                            <p>Tags (seperate with ,)</p>
                            <input className="askInput inputPlaceholder" id="inputTags" defaultValue={this.props.state.tags}/>
                            <input className="btn btn-primary" type="submit" value="Update your question"/>
                            <label className="loginError" ref={label => this.lblLogin = label}> </label>
                        </form>
                    </div>
                </div>
            </div>
        );
    }
}