import React, {Component} from 'react';
import {Link} from 'react-router-dom';


export default class Login extends Component {
    constructor(props) {
        super(props);

        this.handleLogin = this.handleLogin.bind(this);
        this.handleLogout = this.handleLogout.bind(this);
    }

    handleLogin (event){
        event.preventDefault();
        this.props.Auth.login(event.target.username.value.toLowerCase(),event.target.password.value).then(response => {
            console.log("Authentication:", response.msg);

            if(response.msg === "User authenticated successfully")
            {
                this.props.ChangeLoginState(true);
            }
            else{
                this.props.ChangeLoginState(false);
                this.lblLogin.hidden = false;
                this.lblLogin.innerHTML = response.msg;
            }
        }).catch(error => {
            // TODO: Inform the user about the error
            this.props.ChangeLoginState(false);
            console.error("Error authenticating:", error);
        });
    };

    handleLogout (event) {
        event.preventDefault();
        this.props.Auth.logout();
        this.props.ChangeLoginState(false);
    }

    render() {
        let content = <p> Test </p>;
        if(this.props.isLoggedIn === true)
        {
            content =
                <div className="loginContent container">
                    <form className="loginForm" onSubmit={this.handleLogout}>
                        <h2> Logout </h2>
                        <button className="btn btn-primary full-btn" type="submit">Logout</button>
                    </form>
                </div>;
        }
        else
        {
            content =
            <div className="loginContent container">
                <form className="loginForm" onSubmit={this.handleLogin}>
                    <h2> Login </h2>
                    <input type="text" id="username" name="username" placeholder="username"/>
                    <input type="password" id="password" name="password" placeholder="password"/>
                    <input className="btn btn-primary" type="submit" value="Login"/>
                    <label className="loginErrorBottom" hidden={true} ref={label => this.lblLogin = label}> </label>
                </form>
                <Link to={"/createUser"}>
                     <label className="createAccount">Not a member? Sign up!</label>
                </Link>
            </div>;
        }
        return content;

    }
}

