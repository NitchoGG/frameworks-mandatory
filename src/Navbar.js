import React, {Component} from 'react';
import { Link } from 'react-router-dom';

export default class Navbar extends Component {
    constructor(props) {
        super(props);
    }

    render() {
    let status ="";
    let userText ="";
    if(this.props.isLoggedIn) {
        status = "Logout";
        userText = "Logged in as " + localStorage.getItem('username');
    }
    else{
        status = "Login";
        userText = "";
    }

        return (
            <nav className="navbar navbar-light bg-light fixedNav">
                <div className="container-fluid">
                    <Link to={'/'} className="navbar-brand"> Home </Link>
                    <div className={"navbar-nav"}>
                    <Link to={'/allAnswers'} className={"nav-item nav-link"}> All Answers </Link>
                    </div>
                    <div className={"ml-auto mr-1"}>
                        <label className={"userText"}>{userText}</label>
                        <Link to={'/login'} className={"btn btn-primary "}> {status} </Link>
                    </div>
                 </div>
            </nav>
        );
    }
}

