import React, {Component} from 'react';
import {Link,Redirect} from 'react-router-dom';
import QuestionHeader from "./QuestionHeader"
import Answer from "./Answer"

export default class Question extends Component {

    constructor(props) {
        super(props);
        this.props.getQuestion(this.props.id);
        this.props.getAnswers(this.props.id);

        this.state = {
            answerText: "",
            comment: ""
        };

        this.onClickSubmit = this.onClickSubmit.bind(this);
        this.handleOnChange = this.handleOnChange.bind(this);
        this.handleUpdateScore = this.handleUpdateScore.bind(this);
        this.deleteQuestion = this.deleteQuestion.bind(this);
        this.updateViewCounter();
    }

    componentDidMount() {

    }

    updateViewCounter() {
        let question = this.props.state;
        this.props.updateViews(question, this.props.id);
    }

    onClickSubmit(event) {
        event.preventDefault();
        let question = this.props.state;
        let comment = this.state.comment;

        let comments = {
            text: comment,
            author: localStorage.getItem('username'),
            score: 0,
            questionId: this.props.id
        };

        if (!this.props.isLoggedIn) {
            this.lblLogin.innerHTML = "You need to login to post a comment";
        } else {
            this.props.addAnswer(comments, this.props.id);
        }
    }

    handleOnChange(event) {
        this.setState({
            comment: event.target.value
        });
    }

    handleUpdateScore(event) {
        this.props.updateScore(this.props.state, this.props.id, event.target.innerHTML);
    }

    deleteQuestion(event) {
        this.props.deleteQuestion(this.props.state._id);

        this.props.history.push('/');
    }

    render() {
        let question = this.props.state;
        let tagList = [];
        let answers = [];
        let comments = 0;
        let edit = [];

        let content =
            <div className="container">
                <h3>Loading question</h3>
            </div>;

        if (question) {
            let allTags = question.tags;


            for (let i = 0; i < allTags.length; i++) {
                tagList.push(
                    <div className="tagItem">
                        <Link to={`/question/tag/${allTags[i]}`}> {allTags[i]}
                        </Link>
                    </div>
                );
            }

            let user = localStorage.getItem('username');

            if (question.author === user) {
                edit.push(
                    <div className={"editContainer"}>
                        <button className={"btn btn-primary"} onClick={this.deleteQuestion}>Delete</button>
                        <Link to={"/editQuestion"} className={"btn btn-primary"}> Edit </Link>
                    </div>
                )
            }

            if (this.props.answers) {
                this.props.answers.forEach((elm) => {
                    comments++;
                    answers.push(
                        <Answer key={elm._id}
                                answer={elm}
                                questionId={this.props.id}
                                updateScore={this.props.updateAnswerScore}
                                deleteAnswer = {this.props.deleteAnswer}
                                setAnswer = {this.props.setAnswer}
                                comments={comments}/>);
                });
            }

            content = <div className="container">
                <QuestionHeader title={question.title} buttonText={"Ask a question"}/>
                <div className="row questionTop">
                    <div className="col-1">
                        <div className="updateViews">
                            <button onClick={this.handleUpdateScore}>+</button>
                            <h3>{question.score}</h3>
                            <button onClick={this.handleUpdateScore}>-</button>
                        </div>
                    </div>
                    <div className="col-11 questionBottom">
                        {edit}
                        <div className={"row"}>
                            <div className="col-11">
                                <p className={"break"}>{question.questiontext}</p>
                            </div>
                        </div>
                        {tagList}
                        <p className="questionTime">Asked by : {question.author}</p>
                    </div>
                </div>
                <div className="row headerQuestion">
                    <div className="col-12">
                        <h3>{comments} Answers</h3>
                    </div>
                </div>
                {answers}
                <div className="row headerQuestion">
                    <div className="col-12">
                        <form>
                            <h4>Your Answer</h4>
                            <textarea placeholder="Enter your answer" className="txtareaAnswer" id="inputComment"
                                      onChange={this.handleOnChange}>
                            </textarea>
                            <button className="btn btn-primary" onClick={this.onClickSubmit}>Submit answer</button>
                            <label className="loginError" ref={label => this.lblLogin = label}> </label>
                        </form>
                    </div>
                </div>
            </div>
        }

        return content;
    }
}