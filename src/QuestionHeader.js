import React, {Component} from 'react';
import { Link } from 'react-router-dom';

export default class QuestionHeader extends Component {

    constructor(props) {
        super(props);

    }

    render() {

        return (
            <div className="row headerQuestion">
                <div className="col-10">
                    <h3 className={"break"}>{this.props.title}</h3>
                </div>
                <div className="col-2">
                    <Link to={"/ask"} className="btn btn-primary buttonQuestion "> {this.props.buttonText} </Link>
                </div>
            </div>
        );
    }
}