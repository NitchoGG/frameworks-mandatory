import React, {Component} from 'react';
import { Link } from 'react-router-dom';
import QuestionHeader from "./QuestionHeader";
import QuestionListItem from "./QuestionListItem"

export default class QuestionList extends Component {

    constructor(props) {
        super(props);
        this.props.getQuestions();
    }

    render() {
        let list = [];
        let title = "";

        if(this.props.header != null)
        {
            title = "Questions with the tag : " + this.props.header;
        }
        else{
            title = "Newly added questions";
        }

        this.props.questions.forEach((elm) => {
            list.push(<QuestionListItem key={elm._id} question = {elm} Auth = {this.props.Auth} API_URL = {this.props.API_URL} />);
        });

        return (
            <div className="container questions">
                <QuestionHeader title = {title} buttonText = {"Ask a question"} />
                {list}
            </div>
        );
    }
}