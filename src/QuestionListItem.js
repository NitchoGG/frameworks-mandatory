import React, {Component} from 'react';
import { Link } from 'react-router-dom';
import AuthService from "./AuthService";
export default class QuestionListItem extends Component {

    constructor(props) {
        super(props);
        this.state = {
            answers : 0
        };
        this.getAmountOfComments = this.getAmountOfComments.bind(this);
    }

    componentDidMount() {
        this.getAmountOfComments(this.props.question._id);
    }

    getAmountOfComments (id){
        this.props.Auth.fetch(`${this.props.API_URL}/answers/get/` + id)
            .then(answers => {
                let comments = 0;
                answers.forEach((elm) =>{
                    comments++;
                });

                this.showAmount(comments);
            })
            .catch(error => {
                // TODO: Inform the user about the error
                console.error("Error when fetching: ", error);
            });
    };

    showAmount = number =>{
        this.setState({
            answers : number
        });
    };

    render() {
        let tagList =[];
        let allTags = this.props.question.tags;
        let updated = "";

        for(let i = 0; i < allTags.length;i++){
            tagList.push(
                <div className="tagItem">
                    <Link to={`/question/tag/${allTags[i]}`}> {allTags[i]}
                    </Link>
                </div>
            );
        }

        if(this.props.question.updated)
        {
            updated = <p className="questionTime">Updated at: {this.props.question.last_updated}</p>
        }
        else
        {
            updated = <p className="questionTime">Submitted at: {this.props.question.last_updated}</p>
        }
        return (
            <div className="row question">
                <div className="col-2">
                    <div className="views">
                        <p>{this.props.question.views}</p>
                        <p>views</p>
                    </div>
                    <div className="views">
                        <p>{this.state.answers}</p>
                        <p>comments</p>
                    </div>
                </div>
                <div className="col-10">
                    <Link to={`/question/${this.props.question._id}`}>
                        <h3 className={"break"}>{this.props.question.title}</h3>
                    </Link>
                    <div>
                        {tagList}
                        {updated}
                    </div>
                </div>
            </div>
        );
    }
}